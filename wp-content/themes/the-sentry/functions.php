<?php 


// DEFINIR LOS PATHS A LOS DIRECTORIOS DE JAVASCRIPT Y CSS ///////////////////////////



	define( 'JSPATH', get_template_directory_uri() . '/js/' );

	define( 'CSSPATH', get_template_directory_uri() . '/css/' );

	define( 'THEMEPATH', get_template_directory_uri() . '/' );
	
	define( 'SITEURL', site_url('/') );



// FRONT END SCRIPTS AND STYLES //////////////////////////////////////////////////////



	add_action( 'wp_enqueue_scripts', function(){

		// scripts
		wp_enqueue_script( 'plugins', JSPATH.'plugins.js', array('jquery'), '1.0', true );
		wp_enqueue_script( 'functions', JSPATH.'functions.js', array('plugins'), '1.0', true );

		// localize scripts
		wp_localize_script( 'functions', 'ajax_url', admin_url('admin-ajax.php') );

		// styles
		wp_enqueue_style( 'styles', get_stylesheet_uri() );

	});



// ADMIN SCRIPTS AND STYLES //////////////////////////////////////////////////////////



	add_action( 'admin_enqueue_scripts', function(){

		// scripts
		wp_enqueue_script( 'admin-js', JSPATH.'admin.js', array('jquery'), '1.0', true );

		// localize scripts
		wp_localize_script( 'admin-js', 'ajax_url', admin_url('admin-ajax.php') );

		// styles
		wp_enqueue_style( 'admin-css', CSSPATH.'admin.css' );

	});


// REMOVE ADMIN BAR FOR NON ADMINS ///////////////////////////////////////////////////



	add_filter( 'show_admin_bar', function($content){
		return ( current_user_can('administrator') ) ? $content : false;
	});



// CAMBIAR EL CONTENIDO DEL FOOTER EN EL DASHBOARD ///////////////////////////////////



	add_filter( 'admin_footer_text', function() {
		echo 'Powered by <a href="http://www.wordpress.org">WordPress</a>';
	});



// POST THUMBNAILS SUPPORT ///////////////////////////////////////////////////////////



	if ( function_exists('add_theme_support') ){
		add_theme_support('post-thumbnails');
	}

	if ( function_exists('add_image_size') ){
		
		add_image_size( 'slide_home', 1920, 552, true );
		
		// cambiar el tamaño del thumbnail
		
		update_option( 'medium_size_h', 446 );
		update_option( 'medium_size_w', 711 );
		update_option( 'medium_crop', false );
		
	}



// POST TYPES, METABOXES, TAXONOMIES AND CUSTOM PAGES ////////////////////////////////



	require_once('inc/post-types.php');


	require_once('inc/metaboxes.php');


	require_once('inc/taxonomies.php');


	require_once('inc/pages.php');
	
	
// MODIFICAR EL MAIN QUERY ///////////////////////////////////////////////////////////



	add_action( 'pre_get_posts', function($query){

		if ( $query->is_main_query() and ! is_admin() ) {
			if( is_home() ){
				$meta_query = array(
								array(
									'key'     => 'noticia-destacada',
									'value'   => 'si',
									'compare' => '='
								)
							);
				$query->set( 'meta_query', $meta_query );
				$query->set( 'meta_key', 'noticia-destacada' );
				$query->set( 'posts_per_page', 4 );
			}

			if( is_post_type_archive('descarga') ){
				$query->set( 'posts_per_page', -1 );
			}
		}
		return $query;

	});


// REMOVE ACCENTS AND THE LETTER Ñ FROM FILE NAMES ///////////////////////////////////



	add_filter( 'sanitize_file_name', function ($filename) {
		$filename = str_replace('ñ', 'n', $filename);
		return remove_accents($filename);
	});



// HELPER METHODS AND FUNCTIONS //////////////////////////////////////////////////////



	/**
	 * Print the <title> tag based on what is being viewed.
	 * @return string
	 */
	function print_title(){
		global $page, $paged;

		wp_title( '|', true, 'right' );
		bloginfo( 'name' );

		// Add a page number if necessary
		if ( $paged >= 2 || $page >= 2 ){
			echo ' | ' . sprintf( __( 'Página %s' ), max( $paged, $page ) );
		}
	}



	/**
	 * Imprime una lista separada por commas de todos los terms asociados al post id especificado
	 * los terms pertenecen a la taxonomia especificada. Default: Category
	 *
	 * @param  int     $post_id
	 * @param  string  $taxonomy
	 * @return string
	 */
	function print_the_terms($post_id, $taxonomy = 'category'){
		$terms = get_the_terms( $post_id, $taxonomy );

		if ( $terms and ! is_wp_error($terms) ){
			$names = wp_list_pluck($terms ,'name');
			echo implode(', ', $names);
		}
	}



	/**
	 * Regresa la url del attachment especificado
	 * @param  int     $post_id
	 * @param  string  $size
	 * @return string  url de la imagen
	 */
	function attachment_image_url($post_id, $size){
		$image_id   = get_post_thumbnail_id($post_id);
		$image_data = wp_get_attachment_image_src($image_id, $size, true);
		return isset($image_data[0]) ? $image_data[0] : '';
	}


	/**
	 * GET DATE TRANSFORM
	 */
	function getDateTransform($fecha){
		$fecha = date("Y-m-d", strtotime($fecha));
		$dias = array('Lunes','Martes','Miercoles','Jueves','Viernes','Sábado','Domingo');
		$dias_recortados = array('Lun','Mar','Mie','Jue','Vie','Sab','Dom');

		$dia_name = $dias[date('N', strtotime($fecha)) - 1];
		$dia_recortado = $dias_recortados[date('N', strtotime($fecha)) - 1];
		$fecha = explode('-', $fecha);
		$mes = array('01' => 'Enero', '02' => 'Febrero', '03' => 'Marzo', '04' => 'Abril', '05' => 'Mayo', '06' =>'Junio', '07' => 'Julio', '08' => 'Agosto', '09' => 'Septiembre', '10' => 'Octubre', '11' => 'Noviembre', '12' => 'Diciembre');

		return array($fecha[2], $mes[$fecha[1]], $fecha[0], $dia_name, $fecha[1], $dia_recortado);
	}



	/**
	 * 	PAGINATION
	 */
	
	function round_num($num, $to_nearest) {
	   /*Round fractions down (http://php.net/manual/en/function.floor.php)*/
	   return floor($num/$to_nearest)*$to_nearest;
	}

	/**	
	 * OPCIONES PARA LA PAGINACION
	 * @return [type] [description]
	 */
	function optionsPagination(){
		$pagenavi_options = array();
	    $pagenavi_options['pages_text'] = ('Página %CURRENT_PAGE% de %TOTAL_PAGES%:');
	    $pagenavi_options['current_text'] = '%PAGE_NUMBER%';
	    $pagenavi_options['page_text'] = '%PAGE_NUMBER%';
	    $pagenavi_options['first_text'] = ('Primera');
	    $pagenavi_options['last_text'] = ('Última');
	    $pagenavi_options['next_text'] = 'Siguiente';
	    $pagenavi_options['prev_text'] = 'Anterior';
	    $pagenavi_options['dotright_text'] = '...';
	    $pagenavi_options['dotleft_text'] = '...';
	    $pagenavi_options['num_pages'] = 10; //continuous block of page numbers
	    $pagenavi_options['always_show'] = 0;
	    $pagenavi_options['num_larger_page_numbers'] = 0;
	    $pagenavi_options['larger_page_numbers_multiple'] = 5;

	    return $pagenavi_options;
	}

	/**
	 * ORDENA LAS DESCARGAS POR FECHA DE ACTUALIZACIÖN
	 * @param  [object] $downloads [objeto con todas las descargas]
	 * @return [array]  $new_arr   [arreglo con las descargas ordenadas]
	 */
	function getDownloadsOrderDate($downloads){
		$new_arr = array();
		foreach ($downloads as $key => $download) {
			$date = getDateTransform($download->post_date);
			$mes_ano = $date[1].$date[2];

			$archivo_descarga = get_post_meta( $download->ID, 'archivo_descarga', true );

			$index = $key + 1;
			$new_arr[$mes_ano][$key]['icon']    = isset($archivo_descarga['icon']) ? $archivo_descarga['icon'] : '';
			$new_arr[$mes_ano][$key]['name']    = isset($archivo_descarga['name']) ? $archivo_descarga['name'] : '';
			$new_arr[$mes_ano][$key]['url']     = isset($archivo_descarga['url']) ? $archivo_descarga['url'] : '';
			$new_arr[$mes_ano][$key]['subtype'] = isset($archivo_descarga['subtype']) ? $archivo_descarga['subtype'] : '';
			$new_arr[$mes_ano][$key]['date']    = $download->post_date;
			$new_arr[$mes_ano][$key]['mes']     = $date[1];
			$new_arr[$mes_ano][$key]['ano']     = $date[2];
			$new_arr[$mes_ano][$key]['id']      = $download->ID;

		}
		
		return $new_arr;
	}


	function downloadAttachment($data){ 
 
		$download_id = isset($data['id']) ? $data['id'] : 0;

		$archivo_descarga = get_post_meta( $download_id, 'archivo_descarga', true );
		
		$file_url = isset($archivo_descarga['url']) ? $archivo_descarga['url'] : '';
		$array_url = explode("wp-content", $file_url);
		$file = WP_CONTENT_DIR.$array_url[1];
		if (file_exists($file) ) {
		    header('Content-Type: application/octet-stream');
		    header('Content-Disposition: attachment; filename="'.basename($file).'"');
		    header('Expires: 0');
		    header('Cache-Control: must-revalidate');
		    header('Pragma: no-cache');
		    header('Content-Length: ' . filesize($file));
		    readfile($file);
		    exit;
		}
	}


	/**	
	 * PAGINACION ARCHIVES
	 * @return [string]         [html con la paginacion]
	 */
	function pagenavi($paged = '', $num_pages = '', $siteUrl = '', $especial = false, $simbol_url = '?') {
	    global $wpdb, $wp_query;
	    
	    $before = '';
	    $after = '';

	    $pagenavi_options = optionsPagination();

	    if (!is_single()) {

	        $paged = $paged == '' ? intval(get_query_var('paged')) : $paged;
	        $max_page = $num_pages == '' ? $wp_query->max_num_pages : $num_pages;
	 
	        if(empty($paged) || $paged == 0) {
	            $paged = 1;
	        }
	 
	        $pages_to_show = intval($pagenavi_options['num_pages']);
	        $larger_page_to_show = intval($pagenavi_options['num_larger_page_numbers']);
	        $larger_page_multiple = intval($pagenavi_options['larger_page_numbers_multiple']);
	        $pages_to_show_minus_1 = $pages_to_show - 1;
	        $half_page_start = floor($pages_to_show_minus_1/2);
	        $half_page_end = ceil($pages_to_show_minus_1/2);
	        $start_page = $paged - $half_page_start;
	 
	        if($start_page <= 0) {
	            $start_page = 1;
	        }
	 
	        $end_page = $paged + $half_page_end;
	        if(($end_page - $start_page) != $pages_to_show_minus_1) {
	            $end_page = $start_page + $pages_to_show_minus_1;
	        }
	        if($end_page > $max_page) {
	            $start_page = $max_page - $pages_to_show_minus_1;
	            $end_page = $max_page;
	        }
	        if($start_page <= 0) {
	            $start_page = 1;
	        }
	 
	        $larger_per_page = $larger_page_to_show*$larger_page_multiple;
	        $larger_start_page_start = (round_num($start_page, 10) + $larger_page_multiple) - $larger_per_page;
	        $larger_start_page_end = round_num($start_page, 10) + $larger_page_multiple;
	        $larger_end_page_start = round_num($end_page, 10) + $larger_page_multiple;
	        $larger_end_page_end = round_num($end_page, 10) + ($larger_per_page);
	 
	        if($larger_start_page_end - $larger_page_multiple == $start_page) {
	            $larger_start_page_start = $larger_start_page_start - $larger_page_multiple;
	            $larger_start_page_end = $larger_start_page_end - $larger_page_multiple;
	        }
	        if($larger_start_page_start <= 0) {
	            $larger_start_page_start = $larger_page_multiple;
	        }
	        if($larger_start_page_end > $max_page) {
	            $larger_start_page_end = $max_page;
	        }
	        if($larger_end_page_end > $max_page) {
	            $larger_end_page_end = $max_page;
	        }
	        if($max_page > 1 || intval($pagenavi_options['always_show']) == 1) {

	            $pages_text = str_replace("%CURRENT_PAGE%", number_format_i18n($paged), $pagenavi_options['pages_text']);
	            $pages_text = str_replace("%TOTAL_PAGES%", number_format_i18n($max_page), $pages_text);
	            echo $before.'<div class="pagenavi">'."\n";
	 
	            if(!empty($pages_text)) {
	                echo '<span class="no-pagination">'.$pages_text.'</span><ul class="pagination">';
	            }

	            echo '<li class="pag-anterior">';
	            	if ($especial == true) {
	            		$pa = $paged - 1;
	            		echo $paged > 1 ? '<a href="'.$siteUrl.$simbol_url.'pagina='.$pa.'">Anterior</a>' : '';
	            	}else{
	            		previous_posts_link($pagenavi_options['prev_text']);
	            	}
	            	
	            echo '</li>';
	 
	            if ($start_page >= 2 && $pages_to_show < $max_page) {
	                $first_page_text = str_replace("%TOTAL_PAGES%", number_format_i18n($max_page), $pagenavi_options['first_text']);
	                
	                echo '<li><a href="'.$siteUrl.$simbol_url.'" class="first" title="'.$first_page_text.'">1</a></li>';
	                if(!empty($pagenavi_options['dotleft_text'])) {
	                    echo '<li><span class="expand">'.$pagenavi_options['dotleft_text'].'</span></li>';
	                }
	            }
	 
	            if($larger_page_to_show > 0 && $larger_start_page_start > 0 && $larger_start_page_end <= $max_page) {
	                for($i = $larger_start_page_start; $i < $larger_start_page_end; $i+=$larger_page_multiple) {
	                    $page_text = str_replace("%PAGE_NUMBER%", number_format_i18n($i), $pagenavi_options['page_text']);
	                    echo '<li><a href="'.$siteUrl.$simbol_url.'pagina='.$i.'" class="single_page" title="'.$page_text.'">'.$page_text.'</a></li>';
	                }
	            }
	 
	            for($i = $start_page; $i  <= $end_page; $i++) {
	                if($i == $paged) {
	                    $current_page_text = str_replace("%PAGE_NUMBER%", number_format_i18n($i), $pagenavi_options['current_text']);
	                    echo '<li class="num-pag-current"><span class="current">'.$current_page_text.'</span></li>';
	                } else {
	                    $page_text = str_replace("%PAGE_NUMBER%", number_format_i18n($i), $pagenavi_options['page_text']);
	                    $url = $especial == true ? $siteUrl.$simbol_url.'pagina='.$i : esc_url(get_pagenum_link($i));
	                    echo '<li class="num-pag"><a href="'.$url.'" class="single_page" title="'.$page_text.'">'.$page_text.'</a></li>';
	                }
	            }
	 
	            if ($end_page < $max_page) {
	                if(!empty($pagenavi_options['dotright_text'])) {
	                    echo '<li><span class="expand">'.$pagenavi_options['dotright_text'].'</span></li>';
	                }
	                $last_page_text = str_replace("%TOTAL_PAGES%", number_format_i18n($max_page), $pagenavi_options['last_text']);
	                echo '<li><a href="'.$siteUrl.$simbol_url.'pagina='.$max_page.'" class="last" title="'.$last_page_text.'">'.$max_page.'</a></li>';
	            }
	            echo '<li class="pag-siguiente">';
	            	if ($especial == true) {
	            		$pa = $paged + 1;
	            		echo $paged < $num_pages ? '<a href="'.$siteUrl.$simbol_url.'pagina='.$pa.'">Siguiente</a>' : '';
	            	}else{
	            		next_posts_link($pagenavi_options['next_text'], $max_page);
	            	}
	            echo '</li>';
	 
	            if($larger_page_to_show > 0 && $larger_end_page_start < $max_page) {
	                for($i = $larger_end_page_start; $i <= $larger_end_page_end; $i+=$larger_page_multiple) {
	                    $page_text = str_replace("%PAGE_NUMBER%", number_format_i18n($i), $pagenavi_options['page_text']);
	                    echo '<li><a href="'.$siteUrl.$simbol_url.'pagina='.$i.'" class="single_page edsf	" title="'.$page_text.'">'.$page_text.'</a></li>';
	                }
	            }
	            echo '</ul></div>'.$after."\n";
	        }
	    }
	}
